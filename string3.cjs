const getMonthFromDate = (value) => {
  const array = value.split("/");
  const month = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December",
  ];
  const date = new Date(array[2], array[1] - 1, array[0]);

  return month[date.getMonth()];
};

module.exports = getMonthFromDate;
