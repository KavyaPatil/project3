const stringToNumeric = (value) => {
  let number = value.includes("$") ? value.replace("$", "") : value;
  number = number.includes(",") ? number.replaceAll(",", "") : number;
  number = +number;
  //console.log(number)
  if (Number.isNaN(number)) {
    return 0;
  } else {
    return number;
  }
};

module.exports = stringToNumeric;
