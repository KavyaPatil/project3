const arrayToString = (value) => {
  if (value == null || !value.length) {
    return [];
  }
  const sentence = value.join(" ");
  return sentence;
};

module.exports = arrayToString;
