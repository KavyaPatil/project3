const splitIpAdress = (value) => {
  if (value == null) {
    return [];
  }
  let array = value.split(".");

  for (let index = 0; index < array.length; index++) {
    array[index] = +array[index];
    if (Number.isNaN(array[index]) || array[index] < 0 || array[index] > 255) {
      return [];
    }
  }
  if (array.length !== 4) {
    return [];
  }
  return array;
};

module.exports = splitIpAdress;
